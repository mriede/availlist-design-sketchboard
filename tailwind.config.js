module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {
    	colors: {
    		primary: '#f40f78',
    		secondary: '#ffbd04',
    	}
    },
  },
  variants: {},
  plugins: [],
}
