import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	showNavigation: false,
  },
  mutations: {
  	SET_NAV_VISIBLE (state, isVisible) {
  		state.showNavigation = isVisible
  	}
  },
  actions: {
  },
  getters: {
  	isNavVisible: state => state.showNavigation,
  },
  modules: {
  }
})
